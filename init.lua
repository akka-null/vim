-- this config is inspired by ThePrimeagen <3 and kickstart.nvim
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '
require("lazyVim") -- lazy plugin manager
require("remap")
require("set")

-- color schemes
-- require("colors.catppuccin")
-- require("colors.gruvbox")
-- require("colors.nightfox")
require("colors.rose-pine")
-- require("colors.tokyo-night") -- tokyooooooooooooooooooo
-- require("colors.vscode")
-- require("colors.kanagawa")
-- vim.cmd("colorscheme jellybeans")

--                  transparent_background
-- vim.api.nvim_set_hl(0, "Normal", { bg= "none" })
-- vim.api.nvim_set_hl(0, "NormalFloat", { bg= "none" })

-- FIX: you must watch the primeagen video so you can be sure of what some stuff do
-- TODO: isntall harpoon

--[[ install plugins

    use('ThePrimeagen/harpoon')

    --lsp v2
-- ]]
