-- vim.set screen-256color
vim.opt.updatetime = 50 -- need to read more about it 
vim.o.timeoutlen = 300 -- need to check what it does 
-- Sync clipboard between OS and Neovim.
--  Remove this option if you want your OS clipboard to remain independent.
--  See `:help 'clipboard'`
vim.o.clipboard = 'unnamedplus'

-- [[ Highlight on yank ]] i like it a lot
-- See `:help vim.highlight.on_yank()`
local highlight_group = vim.api.nvim_create_augroup('YankHighlight', { clear = true })
vim.api.nvim_create_autocmd('TextYankPost', {
  callback = function()
    vim.highlight.on_yank()
  end,
  group = highlight_group,
  pattern = '*',
})
--
vim.o.mouse = 'a'

vim.opt.nu = true
vim.opt.relativenumber = true

vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true

vim.opt.smartindent = true

vim.opt.wrap = false -- it will not jump into the next line if you keep typing so it will keep going still trying this and see if i like or not onl the time will tell me if i enjoy using it or not btw im just typing at this point to test it XD so fuck yeah

vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv("HOME") .."/.nvim/undodir"
vim.opt.undofile = true

vim.opt.hlsearch = false -- after you done searching for a thing it will unhilight the patteren 
vim.opt.incsearch = true -- it will highlites as you keep typing 

vim.opt.termguicolors = true

--vim.opt.scrolloff= 8 -- it will start going down when you reach the bottom - 8 lines it cool for fast going down and kind it will keep you always in the middle of the page 
vim.opt.scrolloff= 10 -- it will start going down when you reach the bottom - 8 lines it cool for fast going down and kind it will keep you always in the middle of the page 
-- WARN: trying 
vim.opt.signcolumn= 'yes'
--vim.opt.colorcolumn= "150"
vim.opt.isfname:append("@-@")
--
-- Set completeopt to have a better completion experience
vim.o.completeopt = 'menuone,noselect'



--          not using it's for the sake of learning 
--vim.opt.exrc = true  -- when you do vim . it will look for vimrc in that directory and auto source it for you with this youo can have diff setups in diff projects 
--vim.opt.guicursor = ""  -- need to try it it will change from the think cursor into the thick one 
--vim.opt.hidden = true  -- when you open a buffer and quit without saving it it will remane in the memory buffer and you ca nj ump back to it ( it will use some more RAM)

-- The line beneath this is called `modeline`. See `:help modeline`
-- vim: ts=2 sts=2 sw=2 et
