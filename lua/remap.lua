-- my keyboard is fast:
-- when i try to :w ( Shift+w ) i get :W so it wont save the file
-- if you wan "W" hit "ww"
-- if you wan "Q" hit "qq"
-- vim.keymap.set("c", "W", "w")
-- vim.keymap.set("c", "Q", "q")
-- vim.keymap.set("c", "ww", "W")
-- vim.keymap.set("c", "qq", "Q")

vim.keymap.set("n", "<leader>pv", vim.cmd.Ex, { desc = "FileTree" })
-- my fav remap of all time
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")

vim.keymap.set("n", "J", "mzJ`z", { desc = "" }) -- this will alow the cursor to stay at the same place

-- tmuxer keymaps
vim.keymap.set({ "n", "v" }, "<c-f>", ":silent !tmux neww tmuxer<CR>")
-- half page jumping
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")

-- this will alow the search terms keep in the middle
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

-- Diagnostic keymaps
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, { desc = 'Go to previous diagnostic message' })
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, { desc = 'Go to next diagnostic message' })
vim.keymap.set('n', '<leader>e', vim.diagnostic.open_float, { desc = 'Open floating diagnostic message' })
vim.keymap.set('n', '<leader>q', vim.diagnostic.setloclist, { desc = 'Open diagnostics list' })

-- UndotreeToggle
vim.keymap.set("n", "<leader>u", vim.cmd.UndotreeToggle, { desc = "UndotreeToggle" })
-- vim be god
vim.keymap.set("n", "<leader>v", vim.cmd.VimBeGood, { desc = "VimBeGood" })
-- todo comments
vim.keymap.set({ "n", "v" }, "<leader>st", vim.cmd.TodoTelescope, { desc = "TodoTelescope" })


-- greatest remap ever (highlight and paste stuff)
-- vim.keymap.set("x", "<leader>p", [["_dP]], { desc = ""} )

-- next greatest remap ever : asbjornHaland ( copy to clipboard)
vim.keymap.set({ "n", "v" }, "<leader>y", [["+y]], { desc = "CopyToSysClipBoard" })
vim.keymap.set("n", "<leader>Y", [["+Y]], { desc = "CopyToSysClipBoard" })

-- vim.keymap.set({ "n", "v" }, "<leader>d", [["_d]], { desc = ""} )

-- This is going to get me cancelled
-- vim.keymap.set("i", "<C-c>", "<Esc>", { desc = ""} )

-- vim.keymap.set("n", "Q", "<nop>", { desc = ""} )
-- vim.keymap.set("n", "<C-f>", "<cmd>silent !tmux neww tmux-sessionizer<CR>")
vim.keymap.set("n", "<leader>f", vim.lsp.buf.format, { desc = "LspFormat" })

vim.keymap.set("n", "<C-k>", "<cmd>cnext<CR>zz")
vim.keymap.set("n", "<C-j>", "<cmd>cprev<CR>zz")
-- vim.keymap.set("n", "<leader>k", "<cmd>lnext<CR>zz", { desc = ""} )
-- vim.keymap.set("n", "<leader>j", "<cmd>lprev<CR>zz", { desc = ""} )

vim.keymap.set("n", "<leader>rn", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]], { desc = "LspRenameString" })
vim.keymap.set("n", "<leader>x", "<cmd>!chmod +x %<CR>", { silent = true, desc = "Excutable" })
