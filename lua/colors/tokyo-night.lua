vim.g.tokyonight_transparent_sidebar = false
vim.g.tokyonight_transparent = false
vim.opt.background = "dark"

require('lualine').setup {
  options = {
    -- ... your lualine config
    theme = 'tokyonight'
    -- ... your lualine config
  }
}

--vim.cmd("colorscheme tokyonight")

--vim.cmd("colorscheme tokyonight-night")
vim.cmd("colorscheme tokyonight-storm")
--vim.cmd("colorscheme tokyonight-day")
--vim.cmd("colorscheme tokyonight-moon")

