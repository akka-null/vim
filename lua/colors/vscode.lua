vim.cmd('colorscheme vscode')

local colors = {
  black        = '#282828',
  white        = '#D4D4D4',
  red          = '#fb4934',
  green        = '#b8bb26',
  blue         = '#83a598',
  yellow       = '#fe8019',
  darkgray     = '#1E1E1E',
  lightgray    = '#504945',
  inactivegray = '#7c6f64',
}
local dark =  {
  normal = {
    a = {bg = colors.darkgray, fg = colors.white, gui = 'bold'},
    b = {bg = colors.darkgray, fg = colors.white},
    c = {bg = colors.darkgray, fg = colors.white}
  },
  insert = {
    a = {bg = colors.blue, fg = colors.black, gui = 'bold'},
    b = {bg = colors.darkgray, fg = colors.blue},
    c = {bg = colors.darkgray, fg = colors.blue}
  },
  visual = {
    a = {bg = colors.yellow, fg = colors.black, gui = 'bold'},
    b = {bg = colors.darkgray, fg = colors.yellow},
    c = {bg = colors.darkgray, fg = colors.yellow}
  },
  replace = {
    a = {bg = colors.red, fg = colors.black, gui = 'bold'},
    b = {bg = colors.darkgray, fg = colors.red},
    c = {bg = colors.darkgray, fg = colors.red}
  },
  command = {
    a = {bg = colors.green, fg = colors.black, gui = 'bold'},
    b = {bg = colors.lightgray, fg = colors.green},
    c = {bg = colors.darkgray, fg = colors.green}
  },
  inactive = {
    a = {bg = colors.darkgray, fg = colors.gray, gui = 'bold'},
    b = {bg = colors.darkgray, fg = colors.gray},
    c = {bg = colors.darkgray, fg = colors.gray}
  }
}
require('lualine').setup {options = {theme = dark}}

